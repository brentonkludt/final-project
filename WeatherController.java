
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;


public class WeatherController implements Initializable
{
   @FXML
   private Label Brenton;
   
   @FXML
   private ImageView Background;
   
   @FXML
   private TextField LocationEnter;

   @FXML
   private Button Enter;
   
   @FXML
   private Button Clear;
   
   @FXML
   private Label CityState;
   
   @FXML
   private Label Weather;
   
   @FXML
   private Label Temperature;
   
   @FXML
   private Label Wind;
   
   @FXML
   private Label Time;
   
   @FXML
   private Label Pressure;
   
   @FXML
   private Label Visability;
   
   @FXML 
   private Label Invalid;
   
   @FXML
   private ImageView Radar;
   
   @FXML
   private Label RadarDescription;
   
   @FXML
   private Label RadarLocation;
   
   @FXML
   private Label RadarLat;
   
   @FXML
   private Label RadarLong;
   
   @FXML
   private Label WeatherField;
   
   @FXML
   private Label DayFrontTitle;
   
   @FXML
   private Label NightFrontTitle;
   
   @FXML
   private ImageView DayFrontImage;
   
   @FXML
   private Label NightFrontTemp;
   
   @FXML
   private ImageView NightFrontImage;

   @FXML
   private Label NightFrontWeather;
   
   @FXML
   private Label DayZeroTitle;
   
   @FXML
   private Label DayOneTitle;

   @FXML
   private Label DayTwoTitle;

   @FXML
   private Label DayThreeTitle;

   @FXML
   private Label DayFourTitle;
   
   @FXML
   private ImageView DayZeroImage;
   
   @FXML
   private ImageView DayOneImage;

   @FXML
   private ImageView DayTwoImage;  
   
   @FXML
   private ImageView DayThreeImage;

   @FXML
   private ImageView DayFourImage;
   
   @FXML
   private Label DayZeroTemp;
   
   @FXML
   private Label DayOneTemp;
   
   @FXML
   private Label DayTwoTemp;
   
   @FXML
   private Label DayThreeTemp;
   
   @FXML
   private Label DayFourTemp;
   
   @FXML
   private Label ExtDayFiveTemp;
   
   @FXML
   private Label ExtDaySixTemp;
   
   @FXML
   private Label ExtDaySevenTemp;
   
   @FXML
   private Label DayZeroWeather;

   @FXML
   private Label DayOneWeather;   

   @FXML
   private Label DayTwoWeather;
   
   @FXML
   private Label DayThreeWeather;

   @FXML
   private Label DayFourWeather;
   
   @FXML
   private Label DayFiveWeather;
   
   @FXML
   private Label DaySixWeather;
   
   @FXML
   private Label DaySevenWeather;

   @FXML
   private ImageView ExtDayFiveImage;

   @FXML
   private ImageView ExtDaySixImage;
   
   @FXML
   private ImageView ExtDaySevenImage;   
   
   @FXML
   private Label ExtDayFiveTitle;
   
   @FXML
   private Label ExtDaySixTitle;
   
   @FXML
   private Label ExtDaySevenTitle;
   
   
   @FXML
   private void handleButtonAction(ActionEvent e) 
   {
      WeatherModel W = new WeatherModel();

      //Clear
      if (e.getSource() == Clear)
      {
               Brenton.setText(null);
               
               CityState.setText(null);
               Weather.setText(null);
               Temperature.setText(null);
               Background.setImage(null);
               
                              
               WeatherField.setText(null);               
               DayFrontImage.setImage(null);
               NightFrontImage.setImage(null);
               NightFrontTemp.setText(null);
               NightFrontWeather.setText(null);

               
               Invalid.setText(null);               
               Radar.setImage(null);
               RadarLocation.setText(null);
               RadarDescription.setText(null);
               RadarLat.setText(null);
               RadarLong.setText(null);
               
               DayZeroTitle.setText(null);
               DayZeroImage.setImage(null);
               DayZeroTemp.setText(null);
               DayZeroWeather.setText(null);
               
               DayOneTitle.setText(null);
               DayOneImage.setImage(null);
               DayOneTemp.setText(null);
               DayOneWeather.setText(null);
               
               DayTwoTitle.setText(null);
               DayTwoImage.setImage(null);
               DayTwoTemp.setText(null);
               DayTwoWeather.setText(null);
               
               DayThreeTitle.setText(null);
               DayThreeImage.setImage(null);
               DayThreeTemp.setText(null);
               DayThreeWeather.setText(null);
               
               DayFourTitle.setText(null);
               DayFourImage.setImage(null);
               DayFourTemp.setText(null);
               DayFourWeather.setText(null);
               
               ExtDayFiveTitle.setText(null);
               ExtDayFiveImage.setImage(null);
               ExtDayFiveTemp.setText(null);
               DayFiveWeather.setText(null);

               ExtDaySixTitle.setText(null);
               ExtDaySixImage.setImage(null);
               ExtDaySixTemp.setText(null);
               DaySixWeather.setText(null);

               ExtDaySevenTitle.setText(null);
               ExtDaySevenImage.setImage(null); 
               ExtDaySevenTemp.setText(null);
               DaySevenWeather.setText(null);       
      }
      else 
      {
         if (e.getSource() == Enter)
         {
            String zipcode = LocationEnter.getText();
            if (W.getWx(zipcode))
            {
               Brenton.setText("Brenton Kludt, 2018");
               CityState.setText((W.getLocation()));
               Background.setImage(W.getBackground());
            
               Weather.setText((W.getWeather()));
               Temperature.setText(String.valueOf(W.getTemp() + " F"));
               //Wind.setText(W.getWind());
               //Time.setText(W.getTime());
               //Pressure.setText(W.getPressure() + " MB");
               //Visability.setText(W.getVis() + " Miles");
               
               WeatherField.setText(W.getWeatherField());               
               DayFrontImage.setImage(W.getDayFrontImage());
               NightFrontImage.setImage(W.getNightFrontImage());
               NightFrontTemp.setText(W.getNightFrontTemp());
               NightFrontWeather.setText(W.getNightFrontWeather());
               

               
               Invalid.setText("");
               
               Radar.setImage(W.getRadar());
               RadarLocation.setText(W.getRadarLocation());
               RadarDescription.setText("Radar data for: ");
               RadarLat.setText(W.getRadarLat());
               RadarLong.setText(W.getRadarLong());
               
               DayZeroTitle.setText(W.getFiveDayZero());
               DayZeroImage.setImage(W.getFiveDayZeroImage());
               DayZeroTemp.setText(W.getFiveDayZeroTemp());
               DayZeroWeather.setText(W.getDayZeroWeather()); 
               
               
               DayOneTitle.setText(W.getFiveDayOne());
               DayOneImage.setImage(W.getFiveDayOneImage());
               DayOneTemp.setText(W.getFiveDayOneTemp());
               DayOneWeather.setText(W.getDayOneWeather()); 
               
               DayTwoTitle.setText(W.getFiveDayTwo());
               DayTwoImage.setImage(W.getFiveDayTwoImage());
               DayTwoTemp.setText(W.getFiveDayTwoTemp());
               DayTwoWeather.setText(W.getDayTwoWeather()); 

               
               DayThreeTitle.setText(W.getFiveDayThree());
               DayThreeImage.setImage(W.getFiveDayThreeImage());
               DayThreeTemp.setText(W.getFiveDayThreeTemp());
               DayThreeWeather.setText(W.getDayThreeWeather());
               
               DayFourTitle.setText(W.getFiveDayFour());
               DayFourImage.setImage(W.getFiveDayFourImage());
               DayFourTemp.setText(W.getFiveDayFourTemp());
               DayFourWeather.setText(W.getDayFourWeather());
               
               ExtDayFiveTitle.setText(W.getExtDayFiveTitle());
               ExtDayFiveImage.setImage(W.getExtDayFiveImage());
               ExtDayFiveTemp.setText(W.getExtFiveDayFiveTemp());
               DayFiveWeather.setText(W.getDayFiveWeather());

               ExtDaySixTitle.setText(W.getExtDaySixTitle());
               ExtDaySixImage.setImage(W.getExtDaySixImage());
               ExtDaySixTemp.setText(W.getExtFiveDaySixTemp());
               DaySixWeather.setText(W.getDaySixWeather());

               ExtDaySevenTitle.setText(W.getExtDaySevenTitle());
               ExtDaySevenImage.setImage(W.getExtDaySevenImage());
               ExtDaySevenTemp.setText(W.getExtFiveDaySevenTemp());
               DaySevenWeather.setText(W.getDaySevenWeather());


               
               //image.setImage(W.getImage());
            }
            else
            {
               Invalid.setText("Invalid Location");
            }
         }
      }
   }
   
    @Override
    public void initialize(URL url, ResourceBundle rb) 
    {
    // TODO
    }    

}