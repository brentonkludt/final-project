import org.junit.Assert;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;


public class WeatherModelTest
{
    
   @Test
   public void testGetLocation()
   {
      WeatherModel W = new WeatherModel();
      assertEquals(true, W.getWx("95765"));
   }
   
   @Test
   public void testGetLocation2()
   {
      WeatherModel W = new WeatherModel();
      assertEquals(false, W.getWx("857432059"));
   }

   @Test
   public void testGetTemp()
   {
      WeatherModel W = new WeatherModel();
      W.getWx("95765");
      double x = W.getTemp();
      boolean y;
      if (x > -50 && x < 150)
      y = true;
      else
      y = false;
      
      assertEquals(true, y);
   }
   
   @Test
   public void testGetCity()
   {
      WeatherModel W = new WeatherModel();
      W.getWx("95765");
      assertEquals("Rocklin, CA", W.getLocation());
   }
   
}