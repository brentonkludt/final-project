import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import javafx.scene.image.Image;

public class WeatherModel {
  private JsonElement jse;
  private JsonElement jse2;
  private JsonElement jse3;
  private JsonElement jse4;
  public boolean getWx(String zip)
  {
  
    if (zip.charAt(0) > '9')
    {
      return false;
    }
    else
    {
    try
    {
      URL wuURL = new URL("http://api.wunderground.com/api/517d32876b6ce4f7/conditions/q/" + zip + ".json");
      URL radarURL = new URL("http://api.wunderground.com/api/517d32876b6ce4f7/satellite/q/" + zip + ".json");
      URL fiveDayURL = new URL("http://api.wunderground.com/api/517d32876b6ce4f7/forecast10day/q/+" + zip + ".json");
      URL radarstuff = new URL("http://api.wunderground.com/api/517d32876b6ce4f7/geolookup/q/" + zip + ".json");
      
      
      // Open connection
      InputStream is = wuURL.openStream();
      BufferedReader br = new BufferedReader(new InputStreamReader(is));
      
      InputStream is2 = radarURL.openStream();
      BufferedReader br2 = new BufferedReader(new InputStreamReader(is2));
      
      InputStream is3 = fiveDayURL.openStream();
      BufferedReader br3 = new BufferedReader(new InputStreamReader(is3));
      
      InputStream is4 = radarstuff.openStream();
      BufferedReader br4 = new BufferedReader(new InputStreamReader(is4));
            
      
       // Read the results into a JSON Element
      jse = new JsonParser().parse(br);
      jse2 = new JsonParser().parse(br2);
      jse3 = new JsonParser().parse(br3);
      jse4 = new JsonParser().parse(br4);

      // Close connection
      is.close();
      br.close();
      is2.close();
      br2.close();
      is2.close();
      br2.close();
      br3.close();
      is3.close();

      }
    catch (java.io.UnsupportedEncodingException uee)
    {
      uee.printStackTrace();
    }
    catch (java.net.MalformedURLException mue)
    {
      mue.printStackTrace();
    }
    catch (java.io.IOException ioe)
    {
      ioe.printStackTrace();
    }
    catch (java.lang.NullPointerException npe)
    {
      npe.printStackTrace();
    }

    // Check to see if the zip code was valid.
    return isValid();
  }
  }

  public boolean isValid()
  {
    // If the zip is not valid we will get an error field in the JSON
    try {
      String error = jse.getAsJsonObject().get("response").getAsJsonObject().get("error").getAsJsonObject().get("description").getAsString();
      return false;
    }

    catch (java.lang.NullPointerException npe)
    {
      // We did not see error so this is a valid zip
      return true;
    }
    
  }

//METHODS


   public Image getBackground()
   {
   JsonElement z = jse3.getAsJsonObject().get("forecast").getAsJsonObject().get("txt_forecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(0);
   String Day1I = z.getAsJsonObject().get("icon_url").getAsString();
   
   return new Image(Day1I);
   }  


  public String getLocation()
  {
    return jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("display_location").getAsJsonObject().get("full").getAsString();
  }
  
  public String getWeather()
  {
    return jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("weather").getAsString();
  }


  public double getTemp()
  {
    return jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("temp_f").getAsDouble();
  }

  public String getWind()
  {
    return jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("wind_string").getAsString();
  }
  
    public String getTime()
  {
    return jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("observation_time").getAsString();
  }
  
    public String getPressure()
  {
    return jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("pressure_mb").getAsString();
  }
  
    public String getVis()
  {
    return jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("visibility_mi").getAsString();
  }
  
  
  public Image getCurrent()
  {
    String iconURL = jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("icon_url").getAsString();
    return new Image(iconURL);
  }
  
  public Image getRadar()
  {
    String raURL = jse2.getAsJsonObject().get("satellite").getAsJsonObject().get("image_url").getAsString();

    return new Image(raURL);
    
  }
  
  public String getRadarLocation()
  {
    return jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("display_location").getAsJsonObject().get("full").getAsString();
  }
  
  public String getRadarLat()
  {
    return "Latitude:     " + jse4.getAsJsonObject().get("location").getAsJsonObject().get("lat").getAsString();
  }
  
    public String getRadarLong()
  {
    return "Longitude: " + jse4.getAsJsonObject().get("location").getAsJsonObject().get("lon").getAsString();
  }
  
  ///////////////////////////////////
  ///                             ///
  ///        DAY TITLES           ///
  ///                             ///
  ///////////////////////////////////
  
  public String getWeatherField()
  {
   JsonElement z = jse3.getAsJsonObject().get("forecast").getAsJsonObject().get("txt_forecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(0);
   String Day1T = z.getAsJsonObject().get("fcttext").getAsString();
   
   return Day1T;
  }
  
  public String getNightFrontTemp()
  {
   JsonElement z = jse3.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(1);
   String Day1T = z.getAsJsonObject().get("low").getAsJsonObject().get("fahrenheit").getAsString();
   
   return Day1T + " F";
  }   

  public String getNightFrontWeather()
  {
   JsonElement z = jse3.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(1);
   String Day1T = z.getAsJsonObject().get("conditions").getAsString();
   
   return Day1T;
  }   

  public String getDayFrontTitle()
  {
   JsonElement z = jse3.getAsJsonObject().get("forecast").getAsJsonObject().get("txt_forecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(0);
   String Day1T = z.getAsJsonObject().get("title").getAsString();
   
   return Day1T;
  }   
  
  public String getNightFrontTitle()
  {
   JsonElement z = jse3.getAsJsonObject().get("forecast").getAsJsonObject().get("txt_forecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(1);
   String Day1T = z.getAsJsonObject().get("title").getAsString();
   
   return Day1T;
  }   
  
  
  public String getFiveDayZero()
  {
   JsonElement z = jse3.getAsJsonObject().get("forecast").getAsJsonObject().get("txt_forecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(0);
   String Day1T = z.getAsJsonObject().get("title").getAsString();
   
   return Day1T;
  }   
  
  public String getFiveDayOne()
  {
   JsonElement z = jse3.getAsJsonObject().get("forecast").getAsJsonObject().get("txt_forecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(2);
   String Day1T = z.getAsJsonObject().get("title").getAsString();
   
   return Day1T;
  }   
  
  public String getFiveDayTwo()
  {
   JsonElement z = jse3.getAsJsonObject().get("forecast").getAsJsonObject().get("txt_forecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(4);
   String Day1T = z.getAsJsonObject().get("title").getAsString();
   
   return Day1T;
  }   
  
  public String getFiveDayThree()
  {
   JsonElement z = jse3.getAsJsonObject().get("forecast").getAsJsonObject().get("txt_forecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(6);
   String Day1T = z.getAsJsonObject().get("title").getAsString();
   
   return Day1T;
  }  
  
  public String getFiveDayFour()
  {
   JsonElement z = jse3.getAsJsonObject().get("forecast").getAsJsonObject().get("txt_forecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(8);
   String Day1T = z.getAsJsonObject().get("title").getAsString();
   
   return Day1T;
  }   
 
   public Image getExtDayFiveImage()
  {

   JsonElement z = jse3.getAsJsonObject().get("forecast").getAsJsonObject().get("txt_forecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(10);
   String Day1I = z.getAsJsonObject().get("icon_url").getAsString();
   
   return new Image(Day1I);
  }   
   
  public Image getExtDaySixImage()
  {
   JsonElement z = jse3.getAsJsonObject().get("forecast").getAsJsonObject().get("txt_forecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(12);
   String Day1I = z.getAsJsonObject().get("icon_url").getAsString();
   
   return new Image(Day1I);
  } 
  
  public Image getExtDaySevenImage()
  {

   JsonElement z = jse3.getAsJsonObject().get("forecast").getAsJsonObject().get("txt_forecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(14);
   String Day1I = z.getAsJsonObject().get("icon_url").getAsString();
   
   return new Image(Day1I);
  }   

  public String getExtDayFiveTitle()
  {
   JsonElement z = jse3.getAsJsonObject().get("forecast").getAsJsonObject().get("txt_forecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(10);
   String Day1T = z.getAsJsonObject().get("title").getAsString();
   
   return Day1T;
  }   
  
  public String getExtDaySixTitle()
  {
   JsonElement z = jse3.getAsJsonObject().get("forecast").getAsJsonObject().get("txt_forecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(12);
   String Day1T = z.getAsJsonObject().get("title").getAsString();
   
   return Day1T;
  }   
 
  public String getExtDaySevenTitle()
  {
   JsonElement z = jse3.getAsJsonObject().get("forecast").getAsJsonObject().get("txt_forecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(14);
   String Day1T = z.getAsJsonObject().get("title").getAsString();
   
   return Day1T;
  }   
   
  
 
     
  
   
  ///////////////////////////////////
  ///                             ///
  ///        Five Day Images      ///
  ///                             ///
  ///////////////////////////////////


  public Image getDayFrontImage()
  {
   JsonElement z = jse3.getAsJsonObject().get("forecast").getAsJsonObject().get("txt_forecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(0);
   String Day1I = z.getAsJsonObject().get("icon_url").getAsString();
   
   return new Image(Day1I);
  }
  
  public Image getNightFrontImage()
  {
   JsonElement z = jse3.getAsJsonObject().get("forecast").getAsJsonObject().get("txt_forecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(1);
   String Day1I = z.getAsJsonObject().get("icon_url").getAsString();
   
   return new Image(Day1I);
  }
 
  public Image getFiveDayZeroImage()
  {
   JsonElement z = jse3.getAsJsonObject().get("forecast").getAsJsonObject().get("txt_forecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(0);
   String Day1I = z.getAsJsonObject().get("icon_url").getAsString();
   
   return new Image(Day1I);
  }
  
  public Image getFiveDayOneImage()
  {
   JsonElement z = jse3.getAsJsonObject().get("forecast").getAsJsonObject().get("txt_forecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(2);
   String Day1I = z.getAsJsonObject().get("icon_url").getAsString();
   
   return new Image(Day1I);
   
  }
  
  public Image getFiveDayTwoImage()
  {
   JsonElement z = jse3.getAsJsonObject().get("forecast").getAsJsonObject().get("txt_forecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(4);
   String Day1I = z.getAsJsonObject().get("icon_url").getAsString();
   
   return new Image(Day1I);
  }
  
  public Image getFiveDayThreeImage()
  {
   JsonElement z = jse3.getAsJsonObject().get("forecast").getAsJsonObject().get("txt_forecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(6);
   String Day1I = z.getAsJsonObject().get("icon_url").getAsString();
   
   return new Image(Day1I);
  }
  
  public Image getFiveDayFourImage()
  {
   JsonElement z = jse3.getAsJsonObject().get("forecast").getAsJsonObject().get("txt_forecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(8);
   String Day1I = z.getAsJsonObject().get("icon_url").getAsString();

   return new Image(Day1I);
  }
  
  


  ///////////////////////////////////
  ///                             ///
  ///        Five Day Temps       ///
  ///                             ///
  ///////////////////////////////////

  public String getFiveDayZeroTemp()
  {
   JsonElement z = jse3.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(0);
   String Day1T = z.getAsJsonObject().get("high").getAsJsonObject().get("fahrenheit").getAsString();

   return Day1T;
  }   


  public String getFiveDayOneTemp()
  {
   JsonElement z = jse3.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(1);
   String Day1T = z.getAsJsonObject().get("high").getAsJsonObject().get("fahrenheit").getAsString();

   return Day1T;
  }   
  public String getFiveDayTwoTemp()
  {
   JsonElement z = jse3.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(2);
   String Day1T = z.getAsJsonObject().get("high").getAsJsonObject().get("fahrenheit").getAsString();

   return Day1T;
  }   
  
  public String getFiveDayThreeTemp()
  {
   JsonElement z = jse3.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(3);
   String Day1T = z.getAsJsonObject().get("high").getAsJsonObject().get("fahrenheit").getAsString();

   return Day1T;
  }   
  
  public String getFiveDayFourTemp()
  {
   JsonElement z = jse3.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(4);
   String Day1T = z.getAsJsonObject().get("high").getAsJsonObject().get("fahrenheit").getAsString();

   return Day1T;
  }  
   
  public String getExtFiveDayFiveTemp()
  {
   JsonElement z = jse3.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(5);
   String Day1T = z.getAsJsonObject().get("high").getAsJsonObject().get("fahrenheit").getAsString();

   return Day1T;
  }   
   
  public String getExtFiveDaySixTemp()
  {
   JsonElement z = jse3.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(6);
   String Day1T = z.getAsJsonObject().get("high").getAsJsonObject().get("fahrenheit").getAsString();

   return Day1T;
  }   
  
  public String getExtFiveDaySevenTemp()
  {
   JsonElement z = jse3.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(7);
   String Day1T = z.getAsJsonObject().get("high").getAsJsonObject().get("fahrenheit").getAsString();

   return Day1T;
  }
  
  ///////////////////////////////////
  ///                             ///
  ///    Weather for All Days     ///
  ///                             ///
  ///////////////////////////////////
  
  public String getDayZeroWeather()
  {
   JsonElement z = jse3.getAsJsonObject().get("forecast").getAsJsonObject().get("txt_forecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(0);
   String Day1T = z.getAsJsonObject().get("fcttext").getAsString();

   return Day1T;
  }

  public String getDayOneWeather()
  {
   JsonElement z = jse3.getAsJsonObject().get("forecast").getAsJsonObject().get("txt_forecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(2);
   String Day1T = z.getAsJsonObject().get("fcttext").getAsString();

   return Day1T;
  }
  
    public String getDayTwoWeather()
  {
   JsonElement z = jse3.getAsJsonObject().get("forecast").getAsJsonObject().get("txt_forecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(4);
   String Day1T = z.getAsJsonObject().get("fcttext").getAsString();

   return Day1T;
  }
  
    public String getDayThreeWeather()
  {
   JsonElement z = jse3.getAsJsonObject().get("forecast").getAsJsonObject().get("txt_forecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(6);
   String Day1T = z.getAsJsonObject().get("fcttext").getAsString();

   return Day1T;
  }
  
    public String getDayFourWeather()
  {
   JsonElement z = jse3.getAsJsonObject().get("forecast").getAsJsonObject().get("txt_forecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(8);
   String Day1T = z.getAsJsonObject().get("fcttext").getAsString();

   return Day1T;
  }
  
    public String getDayFiveWeather()
  {
   JsonElement z = jse3.getAsJsonObject().get("forecast").getAsJsonObject().get("txt_forecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(10);
   String Day1T = z.getAsJsonObject().get("fcttext").getAsString();

   return Day1T;
  }
  
    public String getDaySixWeather()
  {
   JsonElement z = jse3.getAsJsonObject().get("forecast").getAsJsonObject().get("txt_forecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(0);
   String Day1T = z.getAsJsonObject().get("fcttext").getAsString();

   return Day1T;
  }
  
    public String getDaySevenWeather()
  {
   JsonElement z = jse3.getAsJsonObject().get("forecast").getAsJsonObject().get("txt_forecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(0);
   String Day1T = z.getAsJsonObject().get("fcttext").getAsString();

   return Day1T;
  }
  
}